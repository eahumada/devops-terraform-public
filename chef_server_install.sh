#!/bin/bash

USER=$1
NAME=$2
LASTNAME=$3
EMAIL=$4
PASSWORD=$5
ORG=$6

CHEF_RPM=https://packages.chef.io/files/current/chef-server/12.16.9/el/7/chef-server-core-12.16.9-1.el7.x86_64.rpm
DK_RPM=https://packages.chef.io/files/stable/chefdk/2.3.1/el/7/chefdk-2.3.1-1.el7.x86_64.rpm

HOST=$(nslookup chef-server|grep Name:|cut -f2 -d$'\t')

{
  set -x
  #hostnamectl set-hostname $HOST
  #export HOSTNAME=$HOST
  yum -y install ntp git
  systemctl start ntpd.service
  systemctl enable ntpd.service
  #yum -y update
  wget $CHEF_RPM -P /tmp/
  sha256sum /tmp/chef-server-core-12.*.el7.x86_64.rpm
  rpm -Uvh /tmp/chef-server-core-12.*.el7.x86_64.rpm
  chef-server-ctl reconfigure --accept-license
  mkdir -p /home/centos/.chef
  chef-server-ctl user-create $USER $NAME $LASTNAME $EMAIL $PASSWORD  > /home/centos/.chef/$USER.pem
  chef-server-ctl org-create $ORG $ORG --association_user $USER > /home/centos/.chef/$ORG-validator.pem
  chef-server-ctl install chef-manage
  chef-server-ctl reconfigure
  chef-manage-ctl reconfigure --accept-license
  chown -R centos.centos /home/centos/.chef

  wget $DK_RPM -P /tmp/
  sha256sum /tmp/chefdk-2.*.el7.x86_64.rpm
  rpm -Uvh /tmp/chefdk-2.*.el7.x86_64.rpm
  su centos -c "knife configure --server-url https://$HOST/organizations/$ORG --validation-client-name $USER --validation-key /home/centos/.chef/$ORG-validator.pem --user $USER --key /home/centos/.chef/$USER.pem --defaults -y -r '/home/centos/peinau-pci-chef'"
  su centos -c "knife ssl fetch"

} 2>&1 | tee -a /var/log/chef-server-install_$(date +"%m_%d_%Y_%H_%M_%S").log

